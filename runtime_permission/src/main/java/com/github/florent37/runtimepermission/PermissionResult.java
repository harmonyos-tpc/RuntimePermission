package com.github.florent37.runtimepermission;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * PermissionResult
 *
 * @since 2021-03-13
 */
public class PermissionResult {
    /**
     * To get class name
     */
    private static final String TAG = PermissionResult.class.getName();

    private final RuntimePermission runtimePermission;

    /**
     * The list of permissions accepted
     */
    @NotNull
    private final List<String> accepted = new ArrayList<>();

    /**
     * The list of permissions foreverDenied
     */
    @NotNull
    private final List<String> foreverDenied = new ArrayList<>();

    /**
     * The list of permissions Denied
     */
    @NotNull
    private final List<String> denied = new ArrayList<>();

    /**
     * PermissionResult
     *
     * @param runtimePermission {@link RuntimePermission}
     * @param accepted {@link List}
     * @param foreverDenied {@link List}
     * @param denied {@link List}
     */
    protected PermissionResult(RuntimePermission runtimePermission, @Nullable

    final List<String> accepted, @Nullable final List<String> foreverDenied, @Nullable final List<String> denied) {
        this.runtimePermission = runtimePermission;
        if (accepted != null) {
            this.accepted.addAll(accepted);
        }
        if (foreverDenied != null) {
            this.foreverDenied.addAll(foreverDenied);
        }
        if (denied != null) {
            this.denied.addAll(denied);
        }
    }

    /**
     * askAgain() is used when need to ask or not permission again
     */
    public void askAgain() {
        runtimePermission.ask();
    }

    /**
     * isAccepted() used when permission accepted
     *
     * @return denied
     */
    public boolean isAccepted() {
        return foreverDenied.isEmpty() && denied.isEmpty();
    }

    /**
     * used to get runtime permission
     * @return runtimePermission
     */
    @NotNull
    public RuntimePermission getRuntimePermission() {
        return runtimePermission;
    }

    /**
     * used to go to setting menu
     */
    public void goToSettings() {
        runtimePermission.goToSettings();
    }

    /**
     * Used when permission denied
     * @return denied
     */
    public boolean hasDenied() {
        return !denied.isEmpty();
    }
    /**
     * Used when permission denied forever
     * @return foreverDenied
     */
    public boolean hasForeverDenied() {
        return !foreverDenied.isEmpty();
    }

    /**
     * used when accepted permission
     * @return accepted
     */
    @NotNull
    public List<String> getAccepted() {
        return accepted;
    }

    /**
     * used to get the status of permission which is denied forever
     * @return foreverDenied
     */
    @NotNull
    public List<String> getForeverDenied() {
        return foreverDenied;
    }

    /**
     * used to get the status of permission which is denied
     * @return denied
     */
    @NotNull
    public List<String> getDenied() {
        return denied;
    }
}
