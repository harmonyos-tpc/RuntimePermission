package com.github.florent37.runtimepermission.callbacks;

import com.github.florent37.runtimepermission.PermissionResult;

/**
 * ResponseCallback
 *
 * @since 2021-03-13
 */
public interface ResponseCallback {
    /**
     * onResponse
     *
     * @param result {@link PermissionResult}
     */
    void onResponse(PermissionResult result);
}
