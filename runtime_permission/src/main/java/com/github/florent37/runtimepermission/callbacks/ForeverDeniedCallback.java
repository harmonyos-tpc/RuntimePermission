

package com.github.florent37.runtimepermission.callbacks;

import com.github.florent37.runtimepermission.PermissionResult;

/**
 * ForeverDeniedCallback
 *
 * @since 2021-03-13
 */
public interface ForeverDeniedCallback {
    /**
     * onForeverDenied
     *
     * @param result {@link PermissionResult}
     */
    void onForeverDenied(PermissionResult result);
}
