/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.florent37.runtimepermission.slice;

import com.github.florent37.runtimepermission.HiLogs;
import com.github.florent37.runtimepermission.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import java.util.ArrayList;
import java.util.List;

import static com.github.florent37.runtimepermission.RuntimePermission.LIST_PERMISSIONS;

/**
 * PermissionAbilitySlice extends AbilitySlice
 */
public class PermissionAbilitySlice extends AbilitySlice {
    private static final String TAG = PermissionAbilitySlice.class.getName();

    /**
     * Request code
     */
    public static final int REQUEST_CODE = 23;

    private List<String> permissionsList = new ArrayList<>();
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_permission);
        List<String> permissions = intent.getStringArrayListParam(LIST_PERMISSIONS);
        permissionsList.addAll(permissions);
    }

    @Override
    public void onActive() {
        super.onActive();
        HiLogs.e(TAG, "onActive");
        if (permissionsList.size() > 0) {
            final String[] perms = new String[permissionsList.size()];
            permissionsList.toArray(perms);
            HiLogs.e(TAG, "requestPermissionsFromUser");
            requestPermissionsFromUser(perms, REQUEST_CODE);
        } else {
            // this shouldn't happen, but just to be sure
            terminateAbility();
        }
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
