/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.florent37.runtimepermission;

import com.github.florent37.runtimepermission.slice.PermissionAbilitySlice;
import com.github.florent37.runtimepermission.callbacks.PermissionListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import java.util.ArrayList;
import java.util.List;

import static com.github.florent37.runtimepermission.RuntimePermission.getPermissionListener;
import static ohos.bundle.IBundleManager.PERMISSION_DENIED;
import static ohos.bundle.IBundleManager.PERMISSION_GRANTED;
/**
 * PermissionAbility
 *
 * @since 2021-03-13
 */
public class PermissionAbility extends Ability {
    private static final  String TAG = PermissionAbility.class.getName();
    /**
     * callback
     * @param intent {@link Intent}
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(PermissionAbilitySlice.class.getName());
    }

    /**
     * callback
     * @param requestCode{@link int}
     * @param permissions {@link String}
     * @param grantResults{@link int}
     */
    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        PermissionListener listener = getPermissionListener();
        HiLogs.e(TAG, "requestPermissionsFromUser-Ability");
        if (requestCode == PermissionAbilitySlice.REQUEST_CODE && permissions.length > 0 && listener != null) {
            final List<String> acceptedPermissions = new ArrayList<>();
            final List<String> askAgainPermissions = new ArrayList<>();
            final List<String> refusedPermissions = new ArrayList<>();

            for (int i = 0; i < permissions.length; i++) {
                final String permissionName = permissions[i];
                if (grantResults[i] == PERMISSION_GRANTED) {
                    acceptedPermissions.add(permissionName);
                    HiLogs.e(TAG, "Permission Accepted : "+permissionName);
                } else if (grantResults[i] == PERMISSION_DENIED) {
                    if (canRequestPermission(permissionName)) {
                        askAgainPermissions.add(permissionName);
                        HiLogs.e(TAG, "Permission Ask Again : "+permissionName);
                    } else {
                        refusedPermissions.add(permissionName);
                        HiLogs.e(TAG, "Permission Denied : "+permissionName);
                    }
                }
            }
            listener.onRequestPermissionsResult(acceptedPermissions, refusedPermissions, askAgainPermissions);

            terminateAbility();
        }
    }
}
