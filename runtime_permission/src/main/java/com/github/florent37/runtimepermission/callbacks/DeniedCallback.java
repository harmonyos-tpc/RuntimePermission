

package com.github.florent37.runtimepermission.callbacks;

import com.github.florent37.runtimepermission.PermissionResult;

/**
 * DeniedCallback
 *
 * @since 2021-03-13
 */
public interface DeniedCallback {
    /**
     * onDenied
     *
     * @param result {@link PermissionResult}
     */
    void onDenied(PermissionResult result);
}
