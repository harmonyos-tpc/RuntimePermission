/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.florent37.runtimepermission;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.Locale;

/**
 * HiLogs
 *
 * @since 2021-01-27
 */
public class HiLogs {
    /**
     * To get class name
     */
    private static final String TAG = HiLogs.class.getName();

    private static final int DOMAIN_ID = 0x001D00;

    private static final HiLogLabel LABEL = new HiLogLabel(3, DOMAIN_ID, TAG);

    private static final String LOG_FORMAT = "%s %s";

    /**
     * Print error log
     *
     * @param tag {@link String}
     * @param msg log message
     */
    public static void d(String tag, String msg) {
        HiLog.debug(LABEL, String.format(Locale.ROOT, LOG_FORMAT, tag, msg));
    }

    /**
     * Print error log
     *
     * @param tag {@link String}
     * @param msg log message
     */
    public static void i(String tag, String msg) {
        HiLog.info(LABEL, String.format(Locale.ROOT, LOG_FORMAT, tag, msg));
    }

    /**
     * Print error log
     *
     * @param tag {@link String}
     * @param msg log message
     */
    public static void w(String tag, String msg) {
        HiLog.warn(LABEL, String.format(Locale.ROOT, LOG_FORMAT, tag, msg));
    }

    /**
     * Print error log
     *
     * @param tag {@link String}
     * @param msg log message
     */
    public static void e(String tag, String msg) {
        HiLog.error(LABEL, String.format(Locale.ROOT, LOG_FORMAT, tag, msg));
    }
}
