package com.github.florent37.runtimepermission.callbacks;


import com.github.florent37.runtimepermission.PermissionResult;

/**
 * interface AcceptedCallback
 *
 * @since 2021-03-13
 */
public interface AcceptedCallback {
    /**
     * onAccepted
     *
     * @param result {@link PermissionResult}
     */
    void onAccepted(PermissionResult result);
}
