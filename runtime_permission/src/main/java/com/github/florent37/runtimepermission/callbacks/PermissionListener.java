

package com.github.florent37.runtimepermission.callbacks;

import java.util.List;

/**
 * PermissionListener
 *
 * @since 2021-03-13
 */
public interface PermissionListener {
    /**
     * onRequestPermissionsResult
     *
     * @param acceptedPermissions {@link List}
     * @param refusedPermissions {@link List}
     * @param askAgainPermissions {@link List}
     */
    void onRequestPermissionsResult(List<String> acceptedPermissions, List<String> refusedPermissions,
        List<String> askAgainPermissions);
}
