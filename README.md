# RuntimePermission

Runtime Permission for openharmony
Runtime Permission can handle multiple permission at once.

## Runtime Permission Includes

This list properly implements many features which are missing from other implementations. These are
1.Grant Permission
2.Denied Permission
3.Dont Ask Again

# Usage Instructions
```
askPermission(this, SystemPermission.WRITE_USER_STORAGE,
                SystemPermission.READ_CALL_LOG, SystemPermission.CAMERA)
                        }
                    }
                });
```

# Installation tutorial
## Library Dependencies
1.For using RuntimePermission modules in your sample application, add below dependencies to generate hap/har:
Modify entry build.gradle as below :
```
dependencies {
    compile project(path: ':library')
}
```
2.For using RuntimePermission in separate application, add the below dependencies and include  "library.har" in libs folder of "entry" module  :
Modify entry build.gradle as below :
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
}
```
3.For using RuntimePermission from a remote repository in separate application, add the below dependency in entry/build.gradle:
Modify entry build.gradle as below :
```
dependencies {
    implementation 'io.openharmony.tpc.thirdlib:RuntimePermission:1.0.0'
}
```        

## License

        Copyright 2018 florent37, Inc.

        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and
        limitations under the License.