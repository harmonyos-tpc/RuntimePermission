/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.florent37.runtimepermission.slice;

import com.github.florent37.runtimepermission.HiLogs;

import com.github.florent37.runtimepermission.PermissionResult;
import com.github.florent37.runtimepermission.ResourceTable;
import com.github.florent37.runtimepermission.callbacks.PermissionResultCallback;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.security.SystemPermission;

import java.util.List;

import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;

/**
 * MainAbilitySlice extends AbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final String TAG = MainAbilitySlice.class.getName();

    private Button requestPermission;

    private Text resultPermission;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        Component rootView = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_ability_main, null, false);
        if (rootView instanceof ComponentContainer) {
            super.setUIContent((ComponentContainer) rootView);
        }
        if (rootView.findComponentById(ResourceTable.Id_requestView) instanceof Button) {
            requestPermission = (Button) rootView.findComponentById(ResourceTable.Id_requestView);
        }

        if (rootView.findComponentById(ResourceTable.Id_resultView) instanceof Text) {
            resultPermission = (Text) rootView.findComponentById(ResourceTable.Id_resultView);
        }

        requestPermission.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                myMethod();
            }
        });
    }

    private void myMethod() {
        askPermission(this, SystemPermission.WRITE_USER_STORAGE, SystemPermission.READ_CALL_LOG,
            SystemPermission.CAMERA).ask(new PermissionResultCallback() {
            @Override
            public void onAccepted(PermissionResult permissionResult, List<String> accepted) {
                /**
                 * all permissions already granted or just granted
                 */

                HiLogs.e(TAG, "onAccepted");

                /**
                 * your action
                 */
                resultPermission.setText("Accepted :" + accepted);
            }

            @Override
            public void onDenied(PermissionResult permissionResult, List<String> denied, List<String> foreverDenied) {
                if (permissionResult.hasDenied()) {
                    HiLogs.e(TAG, "onDenied");
                    appendText(resultPermission, "Denied :");
                    /**
                     * the list of denied permissions
                     */

                    for (String permission : denied) {
                        appendText(resultPermission, permission);
                    }
                }

                if (permissionResult.hasForeverDenied()) {
                    HiLogs.e(TAG, "forever Denied");
                    appendText(resultPermission, "ForeverDenied :");
                    /**
                     * the list of forever denied permissions, user has check 'never ask again'
                     */

                    for (String permission : foreverDenied) {
                        appendText(resultPermission, permission);
                    }
                    /**
                     * you need to open setting manually if you really need it
                     */
                    permissionResult.goToSettings();
                }
            }
        });
    }

    /**
     * appendText
     *
     * @param textView {@link Text}
     * @param text {@link String}
     */
    public void appendText(Text textView, String text) {
        textView.setText(textView.getText() + " \n" + text);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
